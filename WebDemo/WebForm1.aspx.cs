﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Easy4net.Common;
using Easy4net.DBUtility;
using Easy4net.Entity;

namespace WebDemo
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
            string strSql = "SELECT  * from  employee";
            
            DBHelper dbHelper = DBHelper.GetInstance();
            List<Employee> emList = dbHelper.FindBySql<Employee>(strSql);

            Response.Write(emList.Count);
        }
    }
}